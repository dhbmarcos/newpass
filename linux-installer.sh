#!/bin/bash -e

echo -n 'Loading information... ';

if [ ! -f newpass ] || [ ! -f LICENSE ]
then
    echo "FAILED";
    echo;
    echo "  File 'newpass' not found in '$(pwd)'.";
    echo "  Contact <https://dhbmarcos.gitlab.io/newpass> to support.";
    echo;
    exit 1;
fi;

version="$(grep 'VERSION=' newpass | cut -d '=' -f2 | tr --delete "'")"
size="$(wc -c newpass | cut -d' ' -f1)"
size="$(bc <<< "scale=3; ${size}/1024")"

echo 'Done.';
echo 'The following additional program will be installed:';
echo "  newpass v$version";
echo;
echo "After this operation, an additional $size KiB of disk space will be used.";
echo -n 'Do you want to continue? [Y/n] ';
read;
echo;

REPLY="$(echo "$REPLY" | tr '[:upper:]' '[:lower:]')"
if [ -n "$REPLY" ] && [ ${REPLY:0:1} == 'y' ]
then
    install -v -o root -g root -m 755 newpass /usr/bin;
    install -v -o root -g root -m 755 LICENSE /usr/share/doc/newpass;
else
    echo Aborted.
fi;
