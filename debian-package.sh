#!/bin/bash

root="/tmp/newpass-$(./newpass --script 8)"

version="$(grep 'VERSION=' newpass | cut --delimiter='=' --fields=2 | tr --delete "'")"
description="$(head -3 README.md | tail -1)"

echo "Making Debian package for newpass v$version...";

mkdir --verbose --parent "$root/newpass/DEBIAN";
mkdir --verbose --parent "$root/newpass/usr/bin";
mkdir --verbose --parent "$root/newpass/usr/share/doc";
cp --verbose LICENSE "$root/newpass/usr/share/doc/newpass";
cp --verbose newpass "$root/newpass/usr/bin";

control=''
control+='Package: newpass\n'
control+="Version: $version\n"
control+='Architecture: all\n'
control+='Maintainer: D. H. B. Marcos <contact@dhbmarcos.com>\n'
control+='Depends: bash (>= 5.0-4), coreutils (>=8.30-3)\n'
control+='Section: admin\n'
control+='Priority: optional\n'
control+='Homepage: https://dhbmarcos.gitlab.io/newpass\n'
control+="Description: $description\n\n"

echo -e "$control" > $root/newpass/DEBIAN/control;
file $root/newpass/DEBIAN/control;

dpkg-deb --verbose --build "$root/newpass";

cp --verbose "$root/newpass.deb" ./newpass_${version}_all.deb;
file ./newpass_${version}_all.deb;

echo 'Done.';
