# Password Generator

Create random password.

## How to use

Simply run `newpass`.

~~~bash
newpass
~~~

will return like:

~~~
Type password length: 8

  password: CnT1kK0D

~~~

The program will ask for the password length.

## Usage in scripts

Use `--script` or `-s` option.

The sample below generate a password with 8 bytes in `password` variable.

~~~bash
password="$(newpass --script 8)"
~~~

If you include in your script using the command `source /usr/bin/newpass`, the the behavior will be the same as that executed via the command line.

If you not set your script to executable like using `/bin/bash YOUR-SCRIPT`, use `--script` or `-s` option.

# Installation

## Requirements

- GNU Linux (https://kernel.org/)
- GNU Bash (https://www.gnu.org/software/bash/)
- GNU core utilities (https://www.gnu.org/software/coreutils/)

This application runs on the most common operating systems.


## Linux Installation

1. Clone repository or download files from https://gitlab.com/dhbmarcos/newpass
2. Run:

    ~~~bash
    bash linux-installer.sh;
    ~~~

### Debian

1. Download last release of Debian package from https://gitlab.com/dhbmarcos/newpass/-/releases;
2. Install package using APT. For example, for installation of version 0.0.1, run:

    ~~~bash
    apt install ./newpass_0.0.1_all.deb
    ~~~

## Windows

For Windows Operation Systems, install and run inside MINGW or Cygwin environments.
See more in https://www.cygwin.com or https://sourceforge.net/projects/mingw

## Uninstall

Simply run:

~~~bash
rm /usr/bin/newpass;
rm /usr/share/doc/newpass;
~~~


### Debian

~~~bash
apt purge newpass
~~~

# Command Line Options

## Usage

*newpass* [ **length** [ **characters** ] | *-h* | *--help* | *--script* ]

## length

Length of password in bytes.

## characters

Possible characters for password. By default, characters are [:alnum:]. You can use any characters sequence or any POSIX characters type. All not printable characters will be ignored.

## -h, --help

Show help information.

## --script

Options to use program in scripts.

# Support

https://dhbmarcos.gitlab.io/newpass


# License

2024 © D. H. B. Marcos

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
